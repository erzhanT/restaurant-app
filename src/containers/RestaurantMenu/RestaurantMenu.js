import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchProducts} from "../../store/actions/ProductsActions";
import './RestaurantMenu.css';
const RestaurantMenu = () => {

    const dispatch = useDispatch();
    const products = useSelector(state => state.products);

    useEffect(() => {
        dispatch(fetchProducts())
    }, [dispatch]);

    console.log(products);

    return (
        <div className='menu'>
            {products && Object.keys(products).map((item, index) => {
                return (
                    <div className="item" key={index}>
                        <img src={products[item].image} alt="products"/>
                        <h3>{products[item].name}</h3>
                        <p>{products[item].price}</p>
                        <button>Add to cart</button>
                    </div>
                )
            })}
        </div>
    );
};

export default RestaurantMenu;